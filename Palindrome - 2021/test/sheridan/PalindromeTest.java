package sheridan;

import static org.junit.Assert.*;


import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindrome( ) {
		assertTrue("Not a Palindrone",Palindrome.isPalindrome("dad"));
	}

	@Test
	public void testIsPalindromeNegative( ) {
		assertFalse("Not a Palindrone",Palindrome.isPalindrome("tree"));
		
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		assertTrue("Not a Palindrone",Palindrome.isPalindrome("edit tide"));
		
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		assertFalse("Not a Palindrone",Palindrome.isPalindrome("edit on tree"));
		
	}	
	
}
